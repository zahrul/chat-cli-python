"""
Usage: python run_chat_client.py [options]

Options:
        [Mandatory]
        -u:             Username
        -e:             Endpoint URL

Examples:
    python run_chat_client.py -u zahrul -e wss://vnlgo0830f.execute-api.eu-west-1.amazonaws.com/dev
"""
import sys
import asyncio
from webSocketClient import WebSocketClient
        

if __name__ == '__main__':
    username = None
    endpoint = None
    i = 1
    while i < len(sys.argv):
        opt = sys.argv[i]

        if opt == '-u':
            i += 1
            username = sys.argv[i]
        if opt == '-e':
            i += 1
            endpoint = sys.argv[i]

        i += 1

    if not (username and endpoint):
        print(__doc__)
        sys.exit(0)

    # Creating client object
    try:
        client = WebSocketClient()
        loop = asyncio.get_event_loop()
        # Start connection and get client connection protocol
        connection = loop.run_until_complete(client.connect(username, endpoint))
        # Start msg listener and input listener 
        tasks = [
            asyncio.ensure_future(client.inputMessageFromKeyboard()),
            asyncio.ensure_future(client.receiveMessage(connection)),
        ]

        loop.run_until_complete(asyncio.wait(tasks))
    except KeyboardInterrupt:
        exit()