import websockets
import asyncio
import aioconsole
import json
import os

class WebSocketClient():

    def __init__(self):
        pass

    async def connect(self, userName, endpointUrl):
        '''
            Connecting to webSocket server

            websockets.client.connect returns a WebSocketClientProtocol, which is used to send and receive messages
        '''
        self.connection = await websockets.client.connect(f'{endpointUrl}?userName={userName}')
        if self.connection.open:
            print('Connection established.')
            print('Press Ctrl + C to exit')
            return self.connection

    async def sendMessage(self, message):
        '''
            Sending message to webSocket server
        '''
        payload = { 'action': 'onMessage', 'message': message }
        await self.connection.send(json.dumps(payload))

    async def receiveMessage(self, connection):
        '''
            Receiving all server messages and handling them
        '''
        while True:
            try:
                message = await connection.recv()
                print(str(message))
                await asyncio.sleep(2)
            except websockets.exceptions.ConnectionClosed:
                print('Connection with server closed')
                break
    
    async def inputMessageFromKeyboard(self):
        '''
            Wait for input from user
        '''
        while True:
            try:
                message = await aioconsole.ainput('')
                print("\033[A                             \033[A")
                await self.sendMessage(message)
                await asyncio.sleep(2)
            except websockets.exceptions.ConnectionClosed:
                print('Connection with server closed')
                break

    ## this is not used ##
    async def heartbeat(self, connection):
        '''
        Sending heartbeat to server every 5 seconds
        Ping - pong messages to verify connection is alive
        '''
        while True:
            try:
                await connection.send('ping')
                await asyncio.sleep(5)
            except websockets.exceptions.ConnectionClosed:
                print('Connection with server closed')
                break
