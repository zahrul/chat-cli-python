# AWS Websocket Messaging

> This repo contains Serverless Framework project for a simple AWS Websocket chat app

### Pre-requisite

> 1. Create AWS account from this [link](https://aws.amazon.com/).
> 2. Configure AWS credential and config file settings. You can refer to this [link](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html) on how to do it.
> 3. Create policy for Dynamo DB full access and update the policy name and to be updated in serverless.yml under provider:iamManagedPolicies.
> 4. Install pip
> 5. Install python 3.7 or later

### Setup

> Install required npm packages first.

```shell
$ npm install
```

> Install Serverless Framework globally.

```shell
$ npm install -g serverless@1.73.1
```

> Install required Python packages.

```shell
$ pip install boto3
$ pip install websockets
$ pip install asyncio
$ pip install aioconsole
```

> Install required serverless plugins.

```shell
$ serverless plugin install -n serverless-pseudo-parameters
```

> Deploy into AWS.

```shell
$ serverless deploy --stage dev
```

### Enable ANSI/VT support.
> Run this command from PowerShell.

```shell
$ Set-ItemProperty HKCU:\Console VirtualTerminalLevel -Type DWORD 1
```

> or from cmd.exe (also works from PowerShell).

```shell
$ reg add HKCU\Console /v VirtualTerminalLevel /t REG_DWORD /d 1
```

### Run the chat client
> Run the chat client via cmd.exe or PowerShell.

```shell
$ python run_chat_client.py -u <username> -e <endpoint url>
```
