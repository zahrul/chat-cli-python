import json
import boto3
import os

dynamodb = boto3.client('dynamodb')

def handle(event, context):
    connectionId = event['requestContext']['connectionId']
    userName = event['queryStringParameters']['userName']

    # Insert the connectionId and userName of the connected device to the database
    dynamodb.put_item(TableName=os.environ['SOCKET_CONNECTIONS_TABLE_NAME'], Item={'connectionId': {'S': connectionId}, 'userName': {'S': userName}})

    return {}