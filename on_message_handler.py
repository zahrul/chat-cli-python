import json
import boto3
import os
from datetime import datetime

from boto3.dynamodb.conditions import Key

dynamodb = boto3.client('dynamodb')
dynamodbResource = boto3.resource('dynamodb')


def updateChatCountTable(userConnectionId):
    try:
        response = dynamodb.get_item(TableName=os.environ['CONFIG_TABLE_NAME'], Key={'config': {'S': 'chatCount'}})
        chat_count = int(response['Item']['value']['S']) + 1
        dynamodb.put_item(TableName=os.environ['CONFIG_TABLE_NAME'], Item={'config': {'S': 'chatCount'}, 'value': {'S': str(chat_count)}})
    except:
        dynamodb.put_item(TableName=os.environ['CONFIG_TABLE_NAME'], Item={'config': {'S': 'chatCount'}, 'value': {'S': '1'}})


def handle(event, context):
    message = json.loads(event['body'])['message']
    paginator = dynamodb.get_paginator('scan')
    
    connectionIds = []

    apigatewaymanagementapi = boto3.client('apigatewaymanagementapi', 
    endpoint_url = "https://" + event["requestContext"]["domainName"] + "/" + event["requestContext"]["stage"])

    # Retrieve all connectionIds from the database
    for page in paginator.paginate(TableName=os.environ['SOCKET_CONNECTIONS_TABLE_NAME']):
        connectionIds.extend(page['Items'])

    userConnectionId = event['requestContext']['connectionId']
    table = dynamodbResource.Table(os.environ['SOCKET_CONNECTIONS_TABLE_NAME'])
    response = table.query(KeyConditionExpression=Key('connectionId').eq(userConnectionId))

    # Emit the recieved message to all the connected devices
    if message:
        for connectionId in connectionIds:
            apigatewaymanagementapi.post_to_connection(
                Data= '[' + datetime.now().strftime('%y-%m-%d %H:%M:%S') + '] ' + response['Items'][0]['userName'] + ': ' + message,
                ConnectionId=connectionId['connectionId']['S']
            )

    # Update total chat count
    updateChatCountTable(userConnectionId)

    return {}